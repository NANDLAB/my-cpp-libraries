/*
 * tok.c
 *
 *   Created on: April 19, 2021
 *       Author: dan
 *  Description: Library for a mixture of escape sequence decoding as in C and tokenizing as in bash
 *
 */

#include "tok.h"

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

void print_with_null(char str[], size_t strsize, FILE *stream) {
	for (size_t i = 0; i < strsize; i++) {
		if (!str[i]) {
			fputc('0', stream);
			continue;
		}
		putchar(str[i]);
	}
}

void remove_char(char str[]) {
	size_t len = strlen(str);
	for (size_t i = 0; i < len; i++) {
		str[i] = str[i+1];
	}
}

void insert_char(char str[], char c) {
	for (size_t i = strlen(str)+1; i--; ) {
		str[i+1] = str[i];
	}
	str[0] = c;
}

bool needs_quoting(const char str[]) {
	const size_t len = strlen(str);
	for (size_t i=0; i<len; i++) {
		if (str[i] == ' ') {
			return true;
		}
	}
	return false;
}

char escape_to_char(char c) {
	// If c is not a valid escape character (including '\0' character), 0 is returned.
	if (c == 'a')
		return '\a';
	if (c == 'b')
		return '\b';
	if (c == 'f')
		return '\f';
	if (c == 'n')
		return '\n';
	if (c == 'r')
		return '\r';
	if (c == 't')
		return '\t';
	if (c == 'v')
		return '\v';
	if (strchr("\\"  "\'"  "\""  "\?",  c))
		return c;
	return 0;
}

char char_to_escape(char c) {
	// If c is not a special character which can be escaped (including '\0' character), 0 is returned.
	if (c == '\a')
		return 'a';
	if (c == '\b')
		return 'b';
	if (c == '\f')
		return 'f';
	if (c == '\n')
		return 'n';
	if (c == '\r')
		return 'r';
	if (c == '\t')
		return 't';
	if (c == '\v')
		return 'v';
	if (strchr("\\"  "\'"  "\""  "\?",  c))
		return c;
	return 0;
}

int tokenize(char str[], size_t *tokenc, char *tokenv[], size_t max_tokens) {
	// Decodes escape sequences and tokenizes str in place setting up token pointers.
	// In str, the tokens are guaranteed to be separated by exactly one '\0'.
	// Both ' and " can be used for quoting.

	/**************** Tokenizing Examples ****************/
	//
	// Hello"world" 							->	Helloworld
	// "A traditional \"Hello world\" program"	->	A traditional "Hello world" program
	// A traditional "Hello   world" program	->	A,traditional,Hello   world,program
	// Line1\nLine2								->	Line1[NL]Line2
	// 'He said: "Tell me more!".'				->	He said: "Tell me more!".

	bool prev_token;			// previous character was in a token
	bool token		= false;	// in a token
	bool quote		= false;	// in a quote
	char quote_char;			// char which opened the quote (' or ")
	*tokenc = 0;
	size_t i = 0;
	size_t len = strlen(str);
	int i_inc;
	while (i < len) {
		i_inc = 1;
		prev_token = token;
		// Check, if entered or exited a quote.
		// quote => token
		if (strchr("\"\'", str[i])) {
			if (!quote) {
				quote = true;
				quote_char = str[i];
				token = true;
				remove_char(str+i);
				len--;
				if (str[i] == quote_char) {
					i_inc = 0;
				}
			}
			else if (str[i] == quote_char) {
				quote = false;
				remove_char(str+i);
				len--;
				if (strchr("\"\'", str[i])) {
					i_inc = 0;
				}
			}
		}
		// not whitespace => token
		if (!isspace((unsigned char)str[i])) {
			token = true;
		}
		// whitespace and not quote => not token
		if (!quote && isspace((unsigned char)str[i])) {
			token = false;
		}
		// Parse escape sequence.
		// escape sequence => not whitespace => token
		if (str[i] == '\\') {
			remove_char(str+i);
			len--;
			if (!(str[i] = escape_to_char(str[i]))) {
				return ERROR_INVALID_ESCAPE;
			}
		}
		// first not token, then token <=> new token
		if (!prev_token && token) {
			if (*tokenc >= max_tokens) {
				return ERROR_TOO_MANY_TOKENS;
			}
			tokenv[*tokenc] = str+i;
			(*tokenc)++;
		}
		// first token, then not token <=> end of token
		if (prev_token && !token) {
			str[i] = '\0';
		}
		// Unnecessary whitespace is removed.
		if (!prev_token && !token) {
			remove_char(str+i);
			len--;
			i_inc = 0;
		}
		i += i_inc;
	}
	// Success					: ERROR_NONE
	// max_tokens exceeded		: ERROR_TOO_MANY_TOKENS
	// Missing terminating '"'	: ERROR_INVALID_QUOTE
	// Invalid escape sequence	: ERROR_INVALID_ESCAPE
	return quote ? ERROR_INVALID_QUOTE : ERROR_NONE;
}

void escape_string(char str[], const char do_not_escape[]) {
	// It is the function callers responsibility
	// to ensure that str is long enough to hold
	// its escaped version.
	// The conversion happens in place.
	// Characters in do_not_escape will not be escaped,
	// even if they are special.
	// In the worst case (every character is special)
	// str's length will double.
	size_t len = strlen(str);
	char escape_char;
	for (size_t i = 0; i < len; i++) {
		if (!strchr(do_not_escape, str[i]) && (escape_char = char_to_escape(str[i]))) {
			insert_char(str+i, '\\');
			len++;
			i++;
			str[i] = escape_char;
		}
	}
}

void escape_string_auto(char str[]) {
	// It is the function callers responsibility
	// to ensure that str is long enough to hold
	// its escaped version.
	// "Quotes" are added automatically of there are spaces.
	// In the worst case (every character but one is special)
	// and quoting needed (one space)
	// str's newlen = oldlen * 2 + 1
	size_t len = strlen(str);
	char escape_char;
	bool quote = false;
	for (size_t i = 0; i < len; i++) {
		if (str[i] == ' ') {
			quote = true;
		}
		else {
			escape_char = char_to_escape(str[i]);
			if (escape_char) {
				insert_char(str+i, '\\');
				len++;
				i++;
				str[i] = escape_char;
			}
		}
	}
	if (quote) {
		insert_char(str, '"');
		str[len++] = '"';
		str[len] = '\0';
	}
}

void assemble_from_tokens(char str[], size_t tokenc, const char *tokenv[],
		const char token_separator[], const char token_opener[], const char token_closer[], const char do_not_escape[])
{
	// It is the function callers responsibility
	// to ensure that str is long enough.
	size_t len_str = 0;
	const size_t len_token_separator = strlen(token_separator);
	const size_t len_token_opener = strlen(token_opener);
	const size_t len_token_closer = strlen(token_closer);
	str[0] = '\0';
	for (size_t i = 0; i < tokenc; i++) {
		if (i) {
			strcpy(str+len_str, token_separator);
			len_str += len_token_separator;
		}
		strcpy(str+len_str, token_opener);
		len_str += len_token_opener;
		strcpy(str+len_str, tokenv[i]);
		escape_string(str+len_str, do_not_escape);
		len_str += strlen(str+len_str);
		strcpy(str+len_str, token_closer);
		len_str += len_token_closer;
	}
}

int assemble_from_tokens_auto(char str[], size_t strsize, size_t tokenc, const char *tokenv[])
{
	if (strsize < 1) return 1;
	char *str_iter = str;
	char const *str_iter_end = str + strsize; // One past last element.
	str[0] = '\0';
	for (size_t i = 0; i < tokenc; i++) {
		if (i) {
			if (str_iter+1 >= str_iter_end) return ERROR_ARR_TOO_SMALL;
			*str_iter++ = ' ';
			*str_iter = '\0';
		}
		char *tmp = malloc(2*strlen(tokenv[i])+2);
		if (tmp == NULL) return ERROR_ALLOC_FAILED;
		strcpy(tmp, tokenv[i]);
		escape_string_auto(tmp);
		size_t addlen = strlen(tmp);
		if (str_iter + addlen >= str_iter_end) {
			free(tmp);
			return ERROR_ARR_TOO_SMALL;
		}
		// if (quote) { *str_iter++ = '"'; *str_iter = '\0'; }
		strcpy(str_iter, tmp);
		str_iter += addlen;
		// if (quote) { *str_iter++ = '"'; *str_iter = '\0'; }
		free(tmp);
	}
	return ERROR_NONE;
}
