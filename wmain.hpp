#ifndef WMAIN_HPP_
#define WMAIN_HPP_

#include <locale>
#include <memory>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <exception>
#include <cstring>
#include <string>

int wmain(int argc, wchar_t *argv[]);

int main(int argc, char *argv[]) {
	using namespace std;
	try {
		setlocale(LC_ALL, "");
		vector<wstring> wargv_string(argc);
		vector<wchar_t *> wargv(argc+1);
		for (int i = 0; i < argc; i++) {
			int argvsize = strlen(argv[i])+1;
			wargv_string[i] = wstring(argvsize, L' ');
			wargv[i] = (wchar_t *)wargv_string[i].data();
			size_t conversion_result = mbstowcs(wargv[i], argv[i], argvsize);
			if (conversion_result == SIZE_MAX) {
				wcerr << L"wmain wrapper: Invalid multibyte character sequence in argument." << endl;
				exit(EXIT_FAILURE);
			}
		}
		wargv[argc] = nullptr;
		return wmain(argc, wargv.data());
	}
	catch (exception &e) {
		wcerr	<< L"wmain wrapper: Terminating due to exception: " << e.what() << endl;
		exit(EXIT_FAILURE);
	}
	catch (...) {
		wcerr	<< L"wmain wrapper: Terminating due to exception of undetermined type." << endl;
		exit(EXIT_FAILURE);
	}
	return EXIT_FAILURE;
}

#endif
