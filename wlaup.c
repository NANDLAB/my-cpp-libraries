#include "wlaup.h"
#include <stdlib.h>
#include <string.h>

static const wchar_t W_LAUP_SIGNATURE[] = L"laup04";
// static const wchar_t W_LAUP_ACK[] = L"ack";

/*
unsigned long generate_id() {
	return 0;
}

static void id2wcs(wchar_t *idstr, unsigned long id) {
	swprintf(idstr, 64, L"%lu", &id);
}
*/

static wchar_t* w_laup_generate_id(wchar_t id[], const wchar_t hostname[]) {
	static unsigned counter = 0;
	swprintf(id, 64, L"%s%03u", hostname, counter);
	counter++;
	return id;
}


int w_laup_generate_packet(wchar_t *packet, size_t packet_size, wchar_t *id, const wchar_t *from, const wchar_t *to, size_t cmd_tokenc, const wchar_t *const *cmd_tokenv) {
	const size_t tokenc = cmd_tokenc+4;
	const wchar_t ** const tokenv = malloc(tokenc*sizeof(wchar_t*));
	if (tokenv == NULL) return ERROR_ALLOC_FAILED;
	w_laup_generate_id(id, from);
	tokenv[0] = W_LAUP_SIGNATURE;
	tokenv[1] = id;
	tokenv[2] = from;
	tokenv[3] = to;
	for (size_t i=0; i<cmd_tokenc; i++) {
		tokenv[i+4] = cmd_tokenv[i];
	}
	int ret = w_assemble_from_tokens_auto(packet, packet_size, tokenc, tokenv);
	free(tokenv);
	return ret;
}

/*
int w_laup_generate_ack_packet(wchar_t *packet, size_t packet_size, unsigned long *id, const wchar_t *from, const wchar_t *to, unsigned long received_packet_id) {
	const size_t tokenc = 6;
	const wchar_t *tokenv[6];
	wchar_t id_str[64];
	wchar_t received_packet_id_str[64];
	*id = generate_id();
	id2wcs(id_str, *id);
	id2wcs(received_packet_id_str, received_packet_id);
	tokenv[0] = W_LAUP_SIGNATURE;
	tokenv[1] = id_str;
	tokenv[2] = from;
	tokenv[3] = to;
	tokenv[4] = W_LAUP_ACK;
	tokenv[5] = received_packet_id_str;
	return w_assemble_from_tokens_auto(packet, packet_size, tokenc, tokenv);
}
*/

int w_laup_parse_packet(wchar_t *packet, wchar_t **id, wchar_t **from, wchar_t **to,
//		bool *is_ack, unsigned long *acked_id,
		size_t *cmd_tokenc, wchar_t **cmd_tokenv, size_t max_cmd_tokenc)
{
	const size_t max_tokenc = max_cmd_tokenc+4;
	size_t tokenc;
	wchar_t ** const tokenv = malloc(max_tokenc*sizeof(wchar_t*));
	if (tokenv == NULL) return ERROR_ALLOC_FAILED;
	int ret = w_tokenize(packet, &tokenc, tokenv, max_tokenc);
	*cmd_tokenc = tokenc-4;
	if (ret) {
		free(tokenv);
		return ret;
	}
	if (tokenc < 5 || wcscmp(W_LAUP_SIGNATURE, tokenv[0])) {
		free(tokenv);
		return LAUP_PACKET_NOT_VALID;
	}
	*id = tokenv[1];
	*from = tokenv[2];
	*to   = tokenv[3];
//	*is_ack = !wcscmp(W_LAUP_ACK, tokenv[4]);
//	if (*is_ack) {
//		if (tokenc < 6) {
//			free(tokenv);
//			return LAUP_PACKET_NOT_VALID;
//		}
//		ret = swscanf(tokenv[5], L"%lu", acked_id);
//		if (ret != 1) {
//			free(tokenv);
//			return LAUP_PACKET_NOT_VALID;
//		}
//	}
//	else {
		for (size_t i=0; i < *cmd_tokenc; i++) {
			cmd_tokenv[i] = tokenv[i+4];
		}
//	}
	free(tokenv);
	return ERROR_NONE;
}
