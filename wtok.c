/*
 * wtok.c
 *
 *   Created on: April 19, 2021
 *       Author: dan
 *  Description: Library for a mixture of escape sequence decoding as in C and tokenizing as in bash
 *
 */

#include "wtok.h"

#include <wctype.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

void w_print_with_null(wchar_t str[], size_t strsize) {
	for (size_t i = 0; i < strsize; i++) {
		if (!str[i]) {
			putwchar(L'0');
			continue;
		}
		putwchar(str[i]);
	}
}

void w_remove_char(wchar_t str[]) {
	size_t len = wcslen(str);
	for (size_t i = 0; i < len; i++) {
		str[i] = str[i+1];
	}
}

void w_insert_char(wchar_t str[], wchar_t c) {
	for (size_t i = wcslen(str)+1; i--; ) {
		str[i+1] = str[i];
	}
	str[0] = c;
}

bool w_needs_quoting(const wchar_t str[]) {
	const size_t len = wcslen(str);
	for (size_t i=0; i<len; i++) {
		if (str[i] == L' ') {
			return true;
		}
	}
	return false;
}

wchar_t w_escape_to_char(wchar_t c) {
	// If c is not a valid escape character (including L'\0' character), 0 is returned.
	if (c == L'a')
		return L'\a';
	if (c == L'b')
		return L'\b';
	if (c == L'f')
		return L'\f';
	if (c == L'n')
		return L'\n';
	if (c == L'r')
		return L'\r';
	if (c == L't')
		return L'\t';
	if (c == L'v')
		return L'\v';
	if (wcschr(L"\\"  "\'"  "\""  "\?",  c))
		return c;
	return 0;
}

wchar_t w_char_to_escape(wchar_t c) {
	// If c is not a special character which can be escaped (including L'\0' character), 0 is returned.
	if (c == L'\a')
		return L'a';
	if (c == L'\b')
		return L'b';
	if (c == L'\f')
		return L'f';
	if (c == L'\n')
		return L'n';
	if (c == L'\r')
		return L'r';
	if (c == L'\t')
		return L't';
	if (c == L'\v')
		return L'v';
	if (wcschr(L"\\"  "\'"  "\""  "\?",  c))
		return c;
	return 0;
}

int w_tokenize(wchar_t str[], size_t *tokenc, wchar_t *tokenv[], size_t max_tokens) {
	// Decodes escape sequences and tokenizes str in place setting up token pointers.
	// In str, the tokens are guaranteed to be separated by exactly one L'\0'.
	// Both ' and " can be used for quoting.

	/**************** Tokenizing Examples ****************/
	//
	// Hello"world" 							->	Helloworld
	// "A traditional \"Hello world\" program"	->	A traditional "Hello world" program
	// A traditional "Hello   world" program	->	A,traditional,Hello   world,program
	// Line1\nLine2								->	Line1[NL]Line2
	// 'He said: "Tell me more!".'				->	He said: "Tell me more!".

	bool prev_token;			// previous character was in a token
	bool token		= false;	// in a token
	bool quote		= false;	// in a quote
	wchar_t quote_char;			// char which opened the quote (' or ")
	*tokenc = 0;
	size_t i = 0;
	size_t len = wcslen(str);
	int i_inc;
	while (i < len) {
		i_inc = 1;
		prev_token = token;
		// Check, if entered or exited a quote.
		// quote => token
		if (wcschr(L"\"\'", str[i])) {
			if (!quote) {
				quote = true;
				quote_char = str[i];
				token = true;
				w_remove_char(str+i);
				len--;
				if (str[i] == quote_char) {
					i_inc = 0;
				}
			}
			else if (str[i] == quote_char) {
				quote = false;
				w_remove_char(str+i);
				len--;
				if (wcschr(L"\"\'", str[i])) {
					i_inc = 0;
				}
			}
		}
		// not whitespace => token
		if (!iswspace(str[i])) {
			token = true;
		}
		// whitespace and not quote => not token
		if (!quote && iswspace(str[i])) {
			token = false;
		}
		// Parse escape sequence.
		// escape sequence => not whitespace => token
		if (str[i] == L'\\') {
			w_remove_char(str+i);
			len--;
			if (!(str[i] = w_escape_to_char(str[i]))) {
				return ERROR_INVALID_ESCAPE;
			}
		}
		// first not token, then token <=> new token
		if (!prev_token && token) {
			if (*tokenc >= max_tokens) {
				return ERROR_TOO_MANY_TOKENS;
			}
			tokenv[*tokenc] = str+i;
			(*tokenc)++;
		}
		// first token, then not token <=> end of token
		if (prev_token && !token) {
			str[i] = L'\0';
		}
		// Unnecessary whitespace is removed.
		if (!prev_token && !token) {
			w_remove_char(str+i);
			len--;
			i_inc = 0;
		}
		i += i_inc;
	}
	// Success					: ERROR_NONE
	// max_tokens exceeded		: ERROR_TOO_MANY_TOKENS
	// Missing terminating '"'	: ERROR_INVALID_QUOTE
	// Invalid escape sequence	: ERROR_INVALID_ESCAPE
	return quote ? ERROR_INVALID_QUOTE : ERROR_NONE;
}

void w_escape_string(wchar_t str[], const wchar_t do_not_escape[]) {
	// It is the function callers responsibility
	// to ensure that str is long enough to hold
	// its escaped version.
	// The conversion happens in place.
	// Characters in do_not_escape will not be escaped,
	// even if they are special.
	// In the worst case (every character is special)
	// str's length will double.
	size_t len = wcslen(str);
	wchar_t escape_char;
	for (size_t i = 0; i < len; i++) {
		if (!wcschr(do_not_escape, str[i]) && (escape_char = w_char_to_escape(str[i]))) {
			w_insert_char(str+i, L'\\');
			len++;
			i++;
			str[i] = escape_char;
		}
	}
}

void w_assemble_from_tokens(wchar_t str[], size_t tokenc, const wchar_t *tokenv[],
		const wchar_t token_separator[], const wchar_t token_opener[], const wchar_t token_closer[], const wchar_t do_not_escape[])
{
	// It is the function callers responsibility
	// to ensure that str is long enough.
	size_t len_str = 0;
	const size_t len_token_separator = wcslen(token_separator);
	const size_t len_token_opener = wcslen(token_opener);
	const size_t len_token_closer = wcslen(token_closer);
	str[0] = L'\0';
	for (size_t i = 0; i < tokenc; i++) {
		if (i) {
			wcscpy(str+len_str, token_separator);
			len_str += len_token_separator;
		}
		wcscpy(str+len_str, token_opener);
		len_str += len_token_opener;
		wcscpy(str+len_str, tokenv[i]);
		w_escape_string(str+len_str, do_not_escape);
		len_str += wcslen(str+len_str);
		wcscpy(str+len_str, token_closer);
		len_str += len_token_closer;
	}
}

int w_assemble_from_tokens_auto(wchar_t str[], size_t strsize, size_t tokenc, const wchar_t *tokenv[])
{
	if (strsize < 1) return 1;
	wchar_t *str_iter = str;
	wchar_t const *str_iter_end = str + strsize; // One past last element.
	str[0] = L'\0';
	for (size_t i = 0; i < tokenc; i++) {
		if (i) {
			if (str_iter+1 >= str_iter_end) return ERROR_ARR_TOO_SMALL;
			*str_iter++ = L' ';
			*str_iter = L'\0';
		}
		wchar_t *tmp = malloc((2*wcslen(tokenv[i])+1)*sizeof(wchar_t));
		if (tmp == NULL) return ERROR_ALLOC_FAILED;
		wcscpy(tmp, tokenv[i]);
		bool quote = w_needs_quoting(tmp);
		w_escape_string(tmp, L"");
		size_t tmplen = wcslen(tmp);
		size_t addlen = tmplen + quote * 2;
		if (str_iter + addlen >= str_iter_end) {
			free(tmp);
			return ERROR_ARR_TOO_SMALL;
		}
		if (quote) { *str_iter++ = L'"'; *str_iter = L'\0'; }
		wcscpy(str_iter, tmp);
		str_iter += tmplen;
		if (quote) { *str_iter++ = L'"'; *str_iter = L'\0'; }
		free(tmp);
	}
	return ERROR_NONE;
}
