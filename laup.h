#ifndef LAUP_H_
#define LAUP_H_

#include "tok.h"
#include "laupbase.h"

#ifdef __cplusplus
extern "C" {
#endif

int laup_generate_packet(char *packet, SIZE_T packet_size, char *id, const char *from, const char *to, SIZE_T cmd_tokenc, const char *const *cmd_tokenv);
// int laup_generate_ack_packet(char *packet, SIZE_T packet_size, char *id, const char *from, const char *to, const char *acked_id);
int laup_parse_packet(char *packet, char **id, char **from, char **to,
//		bool *is_ack, char *acked_id,
		SIZE_T *cmd_tokenc, char **cmd_tokenv, SIZE_T max_cmd_tokenc);

#ifdef __cplusplus
}
#endif

#endif
